

/**
 * Fichero: Pajaro20.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */

  public class Pajaro20 {
    
//*** atributos o propiedades ****
  private static int numpajaros=0;
  private char color; //propiedad o atributo color
  private int edad; //propiedad o atributo edad
//*** metodos de la clase ****
  static void nuevopajaro() {
    numpajaros++;
  };
  Pajaro20() {
    color = 'v';
    edad = 0;
    nuevopajaro();
  }
  Pajaro20(char c, int e) {
    color = c;
    edad = e;
    nuevopajaro();
  }
  static void muestrapajaros() {
    System.out.println(numpajaros);
  };
  public static void main(String[] args) {
    Pajaro20 p1,p2;
    p1=new Pajaro20();
    p2=new Pajaro20('a',3);
    p1.muestrapajaros();
    p2.muestrapajaros();
  }
  
}
