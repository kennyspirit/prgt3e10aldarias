
import java.io.BufferedReader;
import java.io.InputStreamReader;


/**
 * Fichero: LeeCaracter.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 28-oct-2013
 */

public class LeeCaracter {


  public static void main(String[] args ) {
    InputStreamReader input = new InputStreamReader(System.in);
    BufferedReader buffer = new BufferedReader(input);
    char caracter;
    
    try {
      System.out.print("Introduce caracter: ");
      caracter = (char) buffer.read();
      System.out.println("Caracter leida: "+caracter);
    }
    catch ( Exception e) {
      e.printStackTrace();
    }
  }

}
/* EJECUCION:
Introduce caracter: a
Caracter leida: a
*/