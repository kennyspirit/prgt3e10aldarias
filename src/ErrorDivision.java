
/**
 * Fichero: ErrorDivision.java
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 29-oct-2013
 */

public class ErrorDivision {
  
  public static void main(String[] args ) {
    int num1=2,num2=0,cociente;
    String linea;
    try {
      cociente=num1/num2;
    } catch ( ArithmeticException ae) {
      System.err.println("Error Aritmetico: "+ae.getMessage());
      return;
    }
    System.out.println("El cociente es: "+cociente);
  }
}

/* EJECUCION:
Error Aritmetico: / by zero
*/